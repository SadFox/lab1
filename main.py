from random import randint, choice, uniform
from copy import copy
from myutils import RuleHelper

import mywindows as W
import numpy as np
import time, math, json
 
root_folder_name = "C:\\root\\"


class Group: 
    __slots__ = ["rule", "name"]
    def __init__(self, rule = [], name = ""):
        self.rule = copy(rule)
        self.name = name

class User: 
    __slots__ = ["rule", "name", "groups"]
    def __init__(self, rule = [], name = "", groups = []):
        self.rule = copy(rule)
        self.name = name
        self.groups = copy(groups)
 
class VirtualSystem:
    __slots__ = ["m_users", "m_groups", "m_file_count", "m_init_ace_count", "m_user_matrix", "m_ace_count", "m_group_matrix"]
    def __init__(self, filename):
        self.__InitializeSystem(filename)

    def __InitializeSystem(self, filename):
        with open(filename) as init_state:
            data = json.load(init_state)

        self.m_file_count = data['files']
        group_count = data['groups']
        user_count = data['users']

        if group_count > user_count:
            raise Exception("[ERROR] Assert group_count > user_count")
        
        self.m_user_matrix = [ RuleHelper.GetNormalDistributedRules(self.m_file_count) for _ in range(user_count)]
        self.m_group_matrix = [ RuleHelper.GetNormalDistributedRules(self.m_file_count) for _ in range(group_count)]
        
        self.m_groups = [Group(self.m_group_matrix[i], "group" + str(i)) for i in range(group_count)]
        self.m_users = [User(self.m_user_matrix[i], "user" + str(i)) for i in range(user_count)]  
        self.m_init_ace_count = self.__GetInitAceCount()
        self.m_ace_count = 0

    def __GetInitAceCount(self):
        ace_count = 0
        
        for user in self.m_users:
            for r in user.rule:
                ace_count = ace_count + 1 if r != 0 else ace_count

        for group in self.m_groups:
            for r in group.rule:
                ace_count = ace_count + 1 if r != 0 else ace_count

        return ace_count

    def __GetGroupsSummaryRule(self, file_idx, user_distribution):
        summary_rule = 0

        for idx in range(len(user_distribution)):
            if user_distribution[idx] == 1:
                summary_rule |= self.m_groups[idx].rule[file_idx]
        return summary_rule

    def __GetGroupsSummaryRuleByUser(self, file_idx, user):
        summary_rule = 0

        for idx in user.groups:
            summary_rule |= self.m_groups[idx].rule[file_idx]
        return summary_rule


    def __GetUserAceCount(self, user, user_distribution):
        ace_count = 0

        for file_idx in range(self.m_file_count):
            summary_rule = self.__GetGroupsSummaryRule(file_idx, user_distribution)
            ace_count += RuleHelper.GetResultAceCount(summary_rule, user.rule[file_idx])
        return ace_count

    def PrintUserDistribution(self):

        print("\nUsers distribution:")
        for group in self.m_groups:
            print("{}".format(group.name))
            is_empty = True
            for user in self.m_users:
                if self.m_groups.index(group) in user.groups:
                    print("  * {}".format(user.name))
                    is_empty = False
            if is_empty == True:
                print("  * {}".format("empty"))
        print()

    def ParseMatrixInformation(self, matrix):
        for ridx, row in enumerate(matrix):
            print("user{}:".format(ridx))
            for fidx, val in enumerate(row):
                g = self.__GetGroupsSummaryRuleByUser(fidx, self.m_users[ridx])
                s = g if g != 0 else "None"
                r = "" if s == "None" else "Allow: {}; Deny: {};".format(val[0], val[1])

                print("  * file{}. Init user primary rule: {}; Summary group rule: {}; {}".format(fidx, self.m_users[ridx].rule[fidx] ,s, r))


    def GetInitAceCount(self):
        return self.m_init_ace_count

    def GetAceCount(self):
        return self.m_ace_count

    def GetPrintInfo(self, user, user_distribution):
        vec = [0 for _ in range(self.m_file_count)]
        
        for file_idx in range(self.m_file_count):
            summary_rule = self.__GetGroupsSummaryRule(file_idx, user_distribution)
            allow, deny = RuleHelper.GetResultRule(summary_rule, user.rule[file_idx])
            vec[file_idx] = (allow, deny)
        return vec


    def OptimizeAce(self, Q_min, Q_max, pulse_spd, npopulation, iter = 500):
        ace_count = 0
        rule_matrix = []

        for user in self.m_users:
            best_user_distribution = self.__OptimizeUserAce(user, Q_min, Q_max, pulse_spd, npopulation, iter)
            vec = self.GetPrintInfo(user, best_user_distribution)
            #print("{} : {}".format(user.name, vec))
            ace_count += self.__GetUserAceCount(user, best_user_distribution)
            rule_matrix.append(vec)

            for idx in range(len(best_user_distribution)):
                if best_user_distribution[idx] == 1:
                    user.groups.append(idx)

        for group in self.m_groups:
            for r in group.rule:
                ace_count = ace_count + 1 if r != 0 else ace_count

        self.m_ace_count = ace_count

        return rule_matrix

    def __OptimizeUserAce(self, user, Q_min, Q_max, pulse_spd, npopulation, iter = 500):
        group_count = len(self.m_groups)
        agents_matrix = [ [randint(0,1) for _ in range(group_count)] for _ in range(npopulation)]
        
        #Q = [0 for _ in range(npopulation)]
        #V = [ [0 for _ in range(group_count)] for _ in range(npopulation)]

        fitness = [self.__GetUserAceCount(user, user_distribution) for user_distribution in agents_matrix]

        fmin = min(fitness)
        best = agents_matrix[fitness.index(fmin)]

        while iter != 0:
            Q = float(0)
            V = float(0)
            for pidx in range(npopulation):
                for gidx in range(group_count):
                    #Q[pidx] = Q_min + (Q_min - Q_max) * randint(0, 100) * 0.01
                    Q = Q_min + (Q_min - Q_max) * randint(0, 100) * 0.01
                    #V[pidx][gidx] = V[pidx][gidx] + (agents_matrix[pidx][gidx] - best[gidx]) * Q[pidx]
                    V = V + (agents_matrix[pidx][gidx] - best[gidx]) * Q

                    #score = abs((2 / math.pi) * math.atan((math.pi / 2) * V[pidx][gidx]))
                    score = abs((2 / math.pi) * math.atan((math.pi / 2) * V))

                    if (randint(0, 100) * 0.01) < score:
                        agents_matrix[pidx][gidx] ^= 1

                    if (randint(0, 100) * 0.01) > pulse_spd:
                        agents_matrix[pidx][gidx] = best[gidx]

                
                fnew = self.__GetUserAceCount(user, agents_matrix[pidx])
                if fnew <= fmin:
                    best = agents_matrix[pidx]
                    fmin = fnew

            iter -= 1
        return best

class RealSystem:
    __slots__ = ["m_vs"]
    def __init__(self):
        self.m_vs = None

    def __del__(self):
        self.Clear()

    def SetVs(self, vs):
        self.m_vs = vs

    def ApplyVsState(self):
        if self.m_vs == None:
            raise Exception("[ERROR] virtual system for real systeam doesn't set")

        W.create_users(self.m_vs.m_users)
        W.create_groups(self.m_vs.m_groups, self.m_vs.m_users)
        W.create_files(root_folder_name, self.m_vs.m_file_count)
        
        for fidx in range(self.m_vs.m_file_count):
             W.set_acl_to_file(fidx, root_folder_name + "file" + str(fidx), self.m_vs.m_groups, self.m_vs.m_users, self.m_vs.m_file_count)

    def Clear(self):
        W.delete_users(self.m_vs.m_users)
        W.delete_groups(self.m_vs.m_groups)
        W.delete_files(root_folder_name, self.m_vs.m_file_count)


def matrix_pretty_print(matrix):

    print("   |  " + "   ".join([str(x) for x in range(len(matrix[0]))]))
    print("—" * 4 * (len(matrix[0]) + 1))

    for ridx, row in enumerate(matrix):
        print(str(ridx) + "  | ", end = '')
        for val in row:
            print(" {}  ".format(val), end = '')
        print()


def main():

    try:
        vs = VirtualSystem("init.json")
        init_ace_count = vs.GetInitAceCount()

        print("\nUSER matrix:")
        matrix_pretty_print(vs.m_user_matrix)
        #print(np.matrix(vs.m_user_matrix))

        print("\nGROUP matrix:")
        matrix_pretty_print(vs.m_group_matrix)
        #print(np.matrix(vs.m_group_matrix))

        print("\n")
        start = time.time()
        rule_matrix = vs.OptimizeAce(0, 2, 0.1, 30, 200)
        end = time.time()

        vs.ParseMatrixInformation(rule_matrix)
        vs.PrintUserDistribution()

        print("[INIT] Total ACE count: {}".format(init_ace_count))
        print("[OPTIMIZE] Total ACE count: {}".format(vs.GetAceCount()))
        print("Execution time is {} sec".format(int(end - start)))

        if vs.GetInitAceCount() > vs.GetAceCount():
            optimize_rate = (1 - vs.GetAceCount() / vs.GetInitAceCount()) * 100
            print("Optimize rate {}%\n".format(int(optimize_rate)))

            #[ВАЖНО]Не раскомменчивать на хостовой машине 
            #rs = RealSystem()
            #rs.SetVs(vs)
            #rs.ApplyVsState()
        else:
            print("No need optimization\n")

    except Exception as ex:
        print(ex)

if __name__ == '__main__':
    main()
