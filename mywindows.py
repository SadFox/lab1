import win32net
import win32netcon
import win32con
import win32file
import win32api
import win32security
import ntsecuritycon as con
from myutils import RuleHelper

def create_user(username, password):
    user_info = {
        "name": username,
        "password": password,
        "priv": win32netcon.USER_PRIV_USER,
        "flags": win32netcon.UF_NORMAL_ACCOUNT | win32netcon.UF_SCRIPT | win32netcon.UF_DONT_EXPIRE_PASSWD,
    }

    try:
        win32net.NetUserAdd(None, 1, user_info)
    except win32net.error as ex:
        print(ex)

def delete_users(users):
    for user in users:
        try:
            win32net.NetUserDel(None, user.name)
        except win32net.error as ex:
            pass

def create_users(users):
    for user in users:
        create_user(user.name, "111")

def create_groups(groups, users):
    for group in groups:
        try:
            win32net.NetLocalGroupAdd(None, 0, {"name": group.name})
        except win32net.error as ex:
            print(ex)

    for user in users:
        try:
            for gidx in user.groups:
                win32net.NetLocalGroupAddMembers(None, groups[gidx].name, 3, [{"domainandname" : user.name}])
        except win32net.error as ex:
            print(ex)

def delete_groups(groups):
    for group in groups:
        try:
            win32net.NetLocalGroupDel(None, group.name)
        except win32net.error as ex:
            pass

def create_file(filename):
    pyhandle = win32file.CreateFile(filename, win32file.GENERIC_WRITE, 0, None,
    win32file.CREATE_NEW, win32file.FILE_ATTRIBUTE_NORMAL, None)
    if pyhandle == win32file.INVALID_HANDLE_VALUE:
        print('File {} creation error'.format(filename))
    else:
        win32api.CloseHandle(pyhandle)

def try_open_file_with_rule(filename, pr_rule):
    try:
        pyhandle = win32file.CreateFile(filename, get_winmask(pr_rule), 0, None,
        win32file.OPEN_EXISTING, win32file.FILE_ATTRIBUTE_NORMAL, None)
        if pyhandle == win32file.INVALID_HANDLE_VALUE:
            return False
        else:
            win32api.CloseHandle(pyhandle)
            return True
    except win32net.error as identifier:
        
        return False

def delete_file(filename):
	win32file.DeleteFile(filename)

def create_files(folder, file_count):
    for fidx in range(file_count):
        try:
            create_file(folder + "file" + str(fidx))
        except win32net.error as ex:
            pass

def delete_files(folder, file_count):
    for fidx in range(file_count):
        try:
            delete_file(folder + "file" + str(fidx))
        except win32net.error as ex:
            pass

def get_winmask(mask):
	to_winmask = {
		0 : 0x00,
		1 : win32con.DELETE,
		2 : win32con.WRITE_DAC,
		3 : win32con.WRITE_DAC | win32con.DELETE,
		4 : win32con.READ_CONTROL,
		5 : win32con.READ_CONTROL | win32con.DELETE,
		6 : win32con.READ_CONTROL | win32con.WRITE_DAC,
		7 : win32con.READ_CONTROL | win32con.WRITE_DAC | win32con.DELETE
	}

	return to_winmask[mask]

def add_ace_to_dacl(dacl, user_sid, group_rule, user_rule):
    win_allow = 0
    win_deny = 0
    
    allow, deny = RuleHelper.GetResultRule(group_rule, user_rule)

    win_allow = get_winmask(allow)
    win_deny = get_winmask(deny)

    if win_allow != 0:
        dacl.AddAccessAllowedAce(win32security.ACL_REVISION, win_allow, user_sid)
    if win_deny != 0:
        dacl.AddAccessDeniedAce(win32security.ACL_REVISION, win_deny, user_sid)

def set_acl_to_file(file_index, filename, groups, users, total_files):
    sd = win32security.GetFileSecurity(filename, win32security.DACL_SECURITY_INFORMATION)
    dacl = win32security.ACL()

    for user in users:
        user_sid = win32security.LookupAccountName("", user.name)[0]
        summary_rule = RuleHelper.GetGroupSummaryRule(file_index, groups, user.groups)
        add_ace_to_dacl(dacl, user_sid, summary_rule, user.rule[file_index])
    
    for group in groups:
        group_sid = win32security.LookupAccountName("", group.name)[0]
        win_mask = get_winmask(group.rule[file_index])
        dacl.AddAccessAllowedAce(win_mask, group_sid)

    sd.SetSecurityDescriptorDacl(1, dacl, 0)
    win32security.SetFileSecurity(filename, win32security.DACL_SECURITY_INFORMATION, sd)