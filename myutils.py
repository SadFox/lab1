import sys, os

class RuleHelper:
    @classmethod
    def __GetAction(cls, g_ace, u_ace):
        conflict_matrix = [ 
                        [-1, 1],
                        [0, -1]
                    ]
        return conflict_matrix[g_ace][u_ace]

    @classmethod
    def GetResultAceCount(cls, group_rule, user_rule):
        result_ace_count = 0

        allow, deny = cls.GetResultRule(group_rule, user_rule)

        if allow != 0:
            result_ace_count += 1
        if deny != 0:
            result_ace_count += 1
        return result_ace_count

    @classmethod
    def GetResultRule(cls, group_rule, user_rule):
        allow = 0
        deny = 0

        for i in range(3):
            action = cls.__GetAction((group_rule >> i) & 1, (user_rule >> i) & 1)

            if action == 1:
                allow |= (1 << i)
            elif action == 0:
                deny |= (1 << i)

        return allow, deny

    @classmethod
    def GetGroupSummaryRule(cls, file_idx, groups, user_groups):
        summary_rule = 0

        for gidx in user_groups:
            summary_rule |= groups[gidx].rule[file_idx]
        return summary_rule

    @classmethod
    def GetNormalDistributedRules(cls, size):
        import scipy.stats as ss
        import numpy as np

        x = np.arange(-4, 4)
        xU, xL = x + 0.5, x - 0.5 
        prob = ss.norm.cdf(xU, scale = 2) - ss.norm.cdf(xL, scale = 2)
        prob = prob / prob.sum()
        nums = np.random.choice(range(0,8), size = size, p = prob)

        return nums